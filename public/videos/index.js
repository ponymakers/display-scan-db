import brief0 from './brief-0.mp4';
import brief1 from './brief-1.mp4';
import brief2 from './brief-2.mp4';

export const BRIEF_VIDEOS = {
    brief_0: "http://192.168.1.60:7777/brief_0.mp4",
    brief_1: "http://192.168.1.60:7777/brief_1.mp4",
    brief_2: "http://192.168.1.60:7777/brief_2.mp4",
    victoire_1: "http://192.168.1.60:7777/victoire_1.mp4",
    victoire_2: "http://192.168.1.60:7777/victoire_2.mp4",
    retour_clara: "http://192.168.1.60:7777/retour_clara.mp4",
};
