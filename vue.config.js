module.exports = {
    css: {
        loaderOptions: {
            sass: {
                data: '@import "@/scss/app.scss";'
            }
        }
    },
    devServer: {
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    }
};
