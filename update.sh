# force pull git
git fetch --all
git reset --hard origin/master

# setup dependencies
npm i

#kill everything listeneing on port 8080
lsof -ti tcp:8080 | xargs kill

# serve on 8080
npm run serve