const PREFIX = 'headquarter';
const BEDROOM_BATHROOM = `bedroom-bathroom`;
const KITCHEN_LIVING_ROOM = `kitchen-living-room`;
const SCAN = 'scan';
const DATABASE = 'database';

export default [
    {
        path: `/${PREFIX}/${BEDROOM_BATHROOM}/${SCAN}`,
        name: 'ScanBedroomBathroom',
        component: () => import(/* webpackChunkName: "scan-bedroom-bathroom" */ '../views/Headquarter/BedroomBathroom/Scan.vue')
    },
    {
        path: `/${PREFIX}/${KITCHEN_LIVING_ROOM}/${SCAN}`,
        name: 'ScanKitchenLivingRoom',
        component: () => import(/* webpackChunkName: "scan-kitchen-living-room" */ '../views/Headquarter/KitchenLivingRoom/Scan.vue')
    },
    {
        path: `/${PREFIX}/${BEDROOM_BATHROOM}/${DATABASE}`,
        name: 'DatabaseBedroomBathroom',
        component: () => import(/* webpackChunkName: "database-bedroom-bathroom" */ '../views/Headquarter/BedroomBathroom/Database.vue')
    },
    {
        path: `/${PREFIX}/${KITCHEN_LIVING_ROOM}/${DATABASE}`,
        name: 'DatabaseKitchenLivingRoom',
        component: () => import(/* webpackChunkName: "database-kitchen-living-room" */ '../views/Headquarter/KitchenLivingRoom/Database.vue')
    },
]
