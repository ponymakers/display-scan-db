import Vue from 'vue'
import Router from 'vue-router'

import bathroomRoutes from './bathroom'
import bedroomRoutes from './bedroom'
import briefingRoutes from './briefing'
import debugRoutes from './debug'
import generalRoutes from './general'
import headquarterRoutes from './headquarter'
import kitchenRoutes from './kitchen'
import livingRoomRoutes from './livingRoom'
import macroRoutes from './macro'
import adminRoutes from './admin'
import corridorRoutes from './corridor'
import badgeRoutes from './badge'

Vue.use(Router);

export default new Router({
  routes: [
      ...bathroomRoutes,
      ...bedroomRoutes,
      ...briefingRoutes,
      ...debugRoutes,
      ...generalRoutes,
      ...headquarterRoutes,
      ...kitchenRoutes,
      ...livingRoomRoutes,
      ...macroRoutes,
      ...adminRoutes,
      ...corridorRoutes,
      ...badgeRoutes,
  ]
})
