const PATH_PREFIX = 'corridor'; 
const NAME_PREFIX = 'Corridor';

export default [
    {
        path: `/${PATH_PREFIX}/velleda`, 
        name: `/${NAME_PREFIX}Velleda`,
        component: () => import(/* webpackChunkName: "corridor-velleda" */ `../views/${NAME_PREFIX}/Velleda`)
    },
]
