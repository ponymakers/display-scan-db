const PREFIX = 'macro';

export default [
    {
        path: `/${PREFIX}/pupitre/:pupitreId`,
        name: 'Pupitre',
        component: () => import(/* webpackChunkName: "MacroGradient" */ '../views/Macro/Gauges.vue')
    },
]
