const PREFIX = 'briefing';

export default [
    {
        path: `/${PREFIX}/television`,
        name: 'BriefingTelevision',
        component: () => import(/* webpackChunkName: "pc" */ '../views/Briefing/Television')
    },
]
