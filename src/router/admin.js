const PATH_PREFIX = 'admin';
const NAME_PREFIX = 'ADMIN';

export default [
    {
        path: `/${PATH_PREFIX}/main`,
        name: `${NAME_PREFIX}Main`,
        component: () => import(/* webpackChunkName: "bedroom-chest" */ '../views/Admin/AdminMain.vue')
    },
]
