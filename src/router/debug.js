const PREFIX = 'debug';

export default [
    {
        path: `/${PREFIX}/control`,
        name: 'Control',
        component: () => import(/* webpackChunkName: "control" */ '../views/Debug/Control.vue')
    },
]
