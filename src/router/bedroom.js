const PATH_PREFIX = 'bedroom';
const NAME_PREFIX = 'Bedroom';

export default [
    {
        path: `/${PATH_PREFIX}/chest`,
        name: `${NAME_PREFIX}Chest`,
        component: () => import(/* webpackChunkName: "bedroom-chest" */ '../views/Bedroom/Chest.vue')
    },
    {
        path: `/${PATH_PREFIX}/pc-interface`,
        name: `${NAME_PREFIX}PCInterface`,
        component: () => import(/* webpackChunkName: "bedroom-pc-interface" */ '../views/Bedroom/PCInterface.vue')
    },
    {
        path: `/${PATH_PREFIX}/thermostat`,
        name: `${NAME_PREFIX}Thermostat`,
        component: () => import(/* webpackChunkName: "bedroom-thermostat" */ '../views/Bedroom/Thermostat.vue')
    },
]
