import Badge from '../views/Badge.vue'

export default [
    {
        path: '/badge',
        name: 'badge',
        component: Badge
    },
]
