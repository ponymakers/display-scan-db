const PATH_PREFIX = 'living-room';
const NAME_PREFIX = 'LivingRoom';

export default [
    {
        path: `/${PATH_PREFIX}/chest`,
        name: `${NAME_PREFIX}Chest`,
        component: () => import(/* webpackChunkName: "living-room-chest" */ '../views/LivingRoom/Chest.vue')
    },
    {
        path: `/${PATH_PREFIX}/television`,
        name: `${NAME_PREFIX}Television`,
        component: () => import(/* webpackChunkName: "living-room-television" */ '../views/LivingRoom/Television.vue')
    },
    {
        path: `/${PATH_PREFIX}/thermostat`,
        name: `${NAME_PREFIX}Thermostat`,
        component: () => import(/* webpackChunkName: "living-room-thermostat" */ '../views/LivingRoom/Thermostat.vue')
    },
]
