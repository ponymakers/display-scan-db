const PATH_PREFIX = 'bathroom';
const NAME_PREFIX = 'Bathroom';

export default [
    {
        path: `/${PATH_PREFIX}/washing-machine-energy-label`,
        name: `${NAME_PREFIX}WashingMachineEnergyLabel`,
        component: () => import(/* webpackChunkName: "bedroom-chest" */ '../views/Bathroom/WashingMachineEnergyLabel.vue')
    },
]
