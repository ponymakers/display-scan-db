const PATH_PREFIX = 'kitchen';
const NAME_PREFIX = 'Kitchen';

export default [
    {
        path: `/${PATH_PREFIX}/dishwasher-energy-label`,
        name: `${NAME_PREFIX}DishwasherEnergyLabel`,
        component: () => import(/* webpackChunkName: "bedroom-chest" */ '../views/Kitchen/DishwasherEnergyLabel.vue')
    },
    {
        path: `/${PATH_PREFIX}/fridge-energy-label`,
        name: `${NAME_PREFIX}FridgeEnergyLabel`,
        component: () => import(/* webpackChunkName: "bedroom-chest" */ '../views/Kitchen/FridgeEnergyLabel.vue')
    },
]
