const destinations = [
  {
    title: "Ambiance Colorado",
    id: "colorado",
    valid: false,
    location: "Les Ocres de Rustrel dans le Vaucluse",
    description: "Découvrez un site exceptionnel et incontournable du Luberon, en Provence. Les ocres de Rustrel offrent un dépaysement qui n’a rien à envier aux déserts américains.",
    imageUrl: require('@/assets/views/bedroom/destinations/colorado.jpg'),
    co2: {
      cars: 111.75
    }
  },
  {
    title: "Ambiance Irlande",
    id: "irlande",
    valid: false,
    location: "Les Falaises d’Etretat en Normandie",
    description: "Les falaises d’Etretat inspirent encore et toujours les voyageurs qui prennent le temps de les contempler. Amoureux de l’Irlande, laissez-vous séduire par ces monuments naturels, majestueux et fragiles à la fois.",
    imageUrl: require('@/assets/views/bedroom/destinations/irlande.jpg'),
    co2: {
      cars: 31.95
    }
  },
  {
    title: "Ambiance Australie",
    id: "australie",
    valid: false,
    location: "Le Lac du Salagou dans l’Hérault",
    description: "La combinaison de phénomènes géologiques du Lac du Salagou donne une surprenante palette de couleurs, de textures et de formes contrastées. Idéal pour les randonneurs et les sportifs des plans d’eau.",
    imageUrl: require('@/assets/views/bedroom/destinations/australie.jpg'),
    co2: {
      cars: 107.1
    }
  },
  {
    title: "Ambiance Thaïlande",
    id: "thailande",
    valid: false,
    location: "Les cascades des Tufs dans le Jura",
    description: "Au cœur du Jura, ces cascades des Tufs propose un spectacle à couper le souffle. Site naturel extraordinaire, convient notamment aux sorties familiales.",
    imageUrl: require('@/assets/views/bedroom/destinations/thailande.jpg'),
    co2: {
      cars: 63
    }
  },
  {
    title: "Ambiance Canada",
    id: "canada",
    valid: true,
    location: "Le Lac de Lispach dans les Vosges",
    description: "Ce lac, d’une superficie de 10 hectares, offre un dépaysement total, digne des grands espaces canadiens. Prenez le temps de vous balader et de profiter de cette nature débordante.",
    imageUrl: require('@/assets/views/bedroom/destinations/canada.jpg'),
    co2: {
      cars: 67.2
    }
  },
  {
    title: "Ambiance Sahara",
    id: "sahara",
    valid: false,
    location: "La Dune du Pilat dans le bassin d’Arcachon",
    description: "Paysage entre terre et océan, la Dune du Pilat s’illustre d’abord par son gigantisme mais aussi par la diversité et la beauté de son environnement. Venez la découvrir.",
    imageUrl: require('@/assets/views/bedroom/destinations/sahara.jpg'),
    co2: {
      cars: 97.65
    }
  }
]

export interface  Destination {
  title: string,
  id: string,
  valid: boolean,
  location: string,
  description: string,
  imageUrl: string,
  co2: {
    cars: number
  }
}

export default destinations as Array<Destination>
