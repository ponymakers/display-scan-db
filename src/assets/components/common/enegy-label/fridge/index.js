import choice0 from './0.png'
import choice1 from './1.png'
import choice2 from './2.png'
import choice3 from './3.png'

export default {
    choice0,
    choice1,
    choice2,
    choice3,
}
