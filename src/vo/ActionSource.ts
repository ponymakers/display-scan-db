export interface ActionSource {
    name: string;
    id: string;
    phaseId: number;
    phaseDuration: number;
    subId: string;

    validationTopic: string;
    validationRequestedValue?: string;
    onStartTopic?: string;
    onStartMessage?: string;
    onEndTopic?: string;
    onEndMessage?: string;
    validationType: "SIGNAL"|"SEQUENCE"|"CUSTOM";

    manipulationValue?: number;
    actionTexte?: string;
    actionLabel?: string;
    recoValue?: number;
    recoTexte?: string;
    remplacementValue?: number;
    remplacementTexte?: string;
    divers?: number;
    diversTexte?: string;
    yesNo?: boolean;
    bddReco?: string;
    cardref?: string;
    validationSound: string;
}
