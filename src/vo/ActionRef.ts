
export default interface ActionRef{
    name: string;
  
    id: string;
    subId: string;
  
    phaseId: number;
    phaseDuration: number;
  
    homeText:string;
    value:number;
  
    actionLabel?: string;
    
    validationTopic: string;
    validationRequestedValue?: string;
    validationType: "SIGNAL"|"SEQUENCE"|"CUSTOM";
  
    onStartTopic?: string;
    onStartMessage?: string;
    onEndTopic?: string;
    onEndMessage?: string;
    
    yesNo: boolean;
  
    bddReco?: string;
    cardRef?: string;
  
    validationSound: string;
  }

