import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import PubSub from './controllers/PubSub'
import VueDraggableResizable from 'vue-draggable-resizable'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faGlobe, faInfoCircle,faFlag,faCrosshairs, faMapMarker, faMap, faDirections, faSearch, faEye, faFire } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

//Font awesome add icon method
library.add(faGlobe, faInfoCircle,faFlag,faCrosshairs, faMapMarker, faMap, faDirections, faSearch, faEye, faFire)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false;

PubSub.init();

Vue.component('vue-draggable-resizable', VueDraggableResizable)

export const EventBus = new Vue()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
