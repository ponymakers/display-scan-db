export const STATES =  {
    ENIGMA_1: 'enigma-1',
    ENIGMA_2: 'enigma-2',
    WAITING: 'waiting',
    GAME: 'game',
    END: 'end',
};
