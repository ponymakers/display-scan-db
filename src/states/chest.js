export const STATES = {
    LOCKED: 'locked',
    ON: 'on',
    VIDEO: 'video',
};

export const UNLOCK_STATES = {
    LOCKED: 'locked',
    ON: 'on',
};

export const NOTIFICATION_STATES = {
    ON: '1',
    OFF: '0'
};
