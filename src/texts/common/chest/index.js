export const UNLOCK_TEXTS = {
    unlock: 'Déverrouiller'
};

export const ACTION_COMPLETE_TEXTS = [
    {
        imgSrc: require('../../../assets/components/pasmal.png')
    },
    {
        imgSrc: require('../../../assets/components/bravo.png')
    },
    {
        imgSrc: require('../../../assets/components/excellent.png')
    }    
];
