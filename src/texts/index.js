import bathroomTexts from './bathroom'
import bedroomTexts from './bedroom'
import briefingTexts from './briefing'
import generalTexts from './general'
import headquarterTexts from './headquarter'
import kitchenTexts from './kitchen'
import livingRoomTexts from './livingRoom'
import macroTexts from './macro'

export {
    bathroomTexts,
    bedroomTexts,
    briefingTexts,
    generalTexts,
    headquarterTexts,
    kitchenTexts,
    livingRoomTexts,
    macroTexts
};

export default {
    ...bathroomTexts,
    ...bedroomTexts,
    ...briefingTexts,
    ...generalTexts,
    ...headquarterTexts,
    ...kitchenTexts,
    ...livingRoomTexts,
    ...macroTexts,
}
