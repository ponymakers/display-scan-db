import conf from './conf';
import SoundPlayer from './SoundPlayer';

export default [
    {
        topic: conf.mqttTopic_chest_action_complete(0),
        payloads:['0', '1', '2'],
        comment:"Envoie l'action complétée"
    },
    {
        topic: conf.mqttTopic_chest_action_complete(1),
        payloads:['0', '1', '2'],
        comment:"Envoie l'action complétée"
    },
    {
        topic: conf.mqttTopic_maletteMode(0),
        payloads:['enigma-1', 'enigma-2', 'waiting', 'game', 'end'],
        comment:"Envoie l'action complétée"
    },
    {
        topic: conf.mqttTopic_maletteMode(1),
        payloads:['enigma-1', 'enigma-2', 'waiting', 'game', 'end'],
        comment:"Envoie l'action complétée"
    },
    {
        topic: conf.mqttTopic_chest_gauge(0),
        payloads: ['18000 25000', '14000 25000', '6000 25000', '0 25000'],
        comment: 'Valeur de la jauge du coffre',
    },
    {
        topic: conf.mqttTopic_chest_gauge(1),
        payloads: ['18000 25000', '14000 25000', '6000 25000', '0 25000'],
        comment: 'Valeur de la jauge du coffre',
    },
    {
        topic: conf.mqttTopic_chest_scan_working(0),
        payloads: ['0', '1'],
        comment: 'Etat du scanner',
    },
    {
        topic: conf.mqttTopic_chest_scan_working(1),
        payloads: ['0', '1'],
        comment: 'Etat du scanner',
    },
    {
        topic: conf.mqttTopic_ActivitySet,
        payloads:["0","1"],
        comment:"Change de produit"
    },
    {
        topic: conf.mqttTopic_PhaseTimestamp(0),
        payloads: [(Date.now()+20*1000*60).toString()],
        comment: 'Timestamp de phase',
    },
    {
        topic: conf.mqttTopic_PhaseTimestamp(1),
        payloads: [(Date.now()+20*1000*60).toString()],
        comment: 'Timestamp de phase',
    },
    {
        topic: conf.mqttTopic_DatabaseTabClick,
        payloads:["0","1","2"],
        comment:"Simule le changement d'onglet dans la Base"},
    {
        topic: conf.mqttTopic_DatabaseProductClick,
        payloads:["0","1","2","3","4"],
        comment:"Simule le changement de produit dans la partie E-commerce"},
    {
        topic: conf.mqttTopic_DatabaseProductPurchase,
        payloads:[""],
        comment:"Simule l'appuie sur le bouton 'achat'"},
    {
        topic: conf.mqttTopic_ScanActivationState,
        payloads:["0", "1"],
        comment:"Active/désactive le bouton scan"},
        ...["0","1","2","3","4"].map((pin) => {
            return {
                topic: conf.mqttTopic_ScanPin.replace(":pin", pin),
                payloads:["0", "1"],
                comment:"Active/désactive la pin "+ pin
            }
        }),
        ...["0","1"].map((i) => {
            return {
                topic: conf.mqttTopic_ScanMessage.replace(":id", i),
                payloads:["", "Voluptate labore ut consequat enim duis pariatur quis ad nulla est."],
                comment:"Affiche un message a l'emplacement du scan "+ i
            }
        }),
    {
        topic: conf.mqttTopic_salon_tv_mode,
        payloads:['on', 'off'],
        comment:"Simule la télécommande de la télé"
    },
    {
        topic: conf.mqttTopic_briefing_tv_src,
        payloads:['', '0', '1', '2'],
        comment:"Gestion des sources vidéos du briefing"
    },
    {
        topic: conf.mqttTopic_chest_mode(0),
        payloads: ['locked', 'on', 'video'],
        comment: 'Gestion du verrouillage du coffre',
    },
    {
        topic: conf.mqttTopic_PhaseTimestamp(0),
        payloads: [(Date.now()+20*1000*60).toString()],
        comment: 'Timestamp de phase',
    },
    {
        topic: conf.mqttTopic_PhaseTimestamp(1),
        payloads: [(Date.now()+20*1000*60).toString()],
        comment: 'Timestamp de phase',
    },
    {
        topic: conf.mqttTopic_energy_label_washing_machine,
        payloads: ['0', '1', '2', '3'],
        comment: 'Etiquette énergétique machine à laver',
    },
    {
        topic: conf.mqttTopic_energy_label_dishwasher,
        payloads: ['0', '1', '2', '3'],
        comment: 'Etiquette énergétique lave vaisselle',
    },
    {
        topic: conf.mqttTopic_energy_label_fridge,
        payloads: ['0', '1', '2', '3'],
        comment: 'Etiquette énergétique lave Frigo',
    },


    {
        topic: conf.mqttTopic_PC_mode_activate,
        payloads: ["optimisation","co2","avion","veille"],
        comment: 'Active une phase du PC',
    },


    {
        topic: conf.mqttTopic_PC_mode_deactivate,
        payloads: ["optimisation","co2","avion","veille"],
        comment: 'désactive une phase du PC',
    },
    {
        topic: conf.mqttTopic_thermostat_mode_programmation(0),
        payloads:['on', 'off'],
        comment:"Change le mode du thermostat dans la chambre-sdb en mode programmation"
    },
    {
        topic: conf.mqttTopic_thermostat_mode_temperature(0),
        payloads:['on', 'off'],
        comment:"Change le mode du thermostat dans la chambre-sdb en mode température"
    },
    {
        topic: conf.mqttTopic_thermostat_mode_programmation(1),
        payloads:['on', 'off'],
        comment:"Change le mode du thermostat dans le salon-cuisine en mode programmation"
    },
    {
        topic: conf.mqttTopic_thermostat_mode_temperature(1),
        payloads:['on', 'off'],
        comment:"Change le mode du thermostat dans le salon-cuisine en mode température"
    },
    ...["0","1","2","3","4","5"].map((id)=>{
        return {
            topic: conf.mqttTopic_Pupitre_Encoder_OnOff("pupitredroit").replace(":boutonId", id),
            payloads:["0", "1"],
            comment:"Active/désactive le bouton "+ id
        }
    }),
    ...["0","1","2","3","4","5"].map((id)=>{
        return {
            topic: conf.mqttTopic_Pupitre_Jauge("pupitredroit").replace(":boutonId", id),
            payloads:["0","1","2","3","4","5"],
            comment:"jauge le bouton"+ id
        }
    }),
{
    topic: conf.mqttTopic_Pupitre_Scan("pupitredroit"),
    payloads:["", ...Array.from(Array(28).keys()).map(i=>i.toString())],
    comment:"simule la pose d'un element a scanner"},
    ...["0","1","2","3","4","5"].map((id)=>{
        return {
            topic: conf.mqttTopic_Pupitre_Encoder_OnOff("pupitregauche").replace(":boutonId", id),
            payloads:["0", "1"],
            comment:"Active/désactive le bouton "+ id}}),
    ...["0","1","2","3","4","5"].map((id)=>{
        return {
            topic: conf.mqttTopic_Pupitre_Jauge("pupitregauche").replace(":boutonId", id),
            payloads:["0","1","2","3","4","5"],
            comment:"jauge le bouton"+ id
        }
    }),
    {
        topic: conf.mqttTopic_Pupitre_Scan("pupitregauche"),
        payloads:["", ...Array.from(Array(28).keys()).map(i=>i.toString())],
        comment:"simule la pose d'un element a scanner"
    },
    {
        topic: "/velleda/messages",
        payloads:[JSON.stringify(['Officia est elit cillum adipisicing.', 'Ad eiusmod ut incididunt ex culpa.'])],
        comment:"Velleda test"
    },
    {
        topic: conf.mqttTopic_AudioRoom(0),
        payloads:[...SoundPlayer.soundRefs.map(ref=>ref.id)],
        comment:"Audio Chambre"
    },
    {
        topic: conf.mqttTopic_AudioRoom(1),
        payloads:[...SoundPlayer.soundRefs.map(ref=>ref.id)],
        comment:"Audio Salon"
    },
    {
        topic: conf.mqttTopic_AudioMalette(0),
        payloads:[...SoundPlayer.soundRefs.map(ref=>ref.id)],
        comment:"Audio Malette Chambre"
    },
    {
        topic: conf.mqttTopic_AudioMalette(1),
        payloads:[...SoundPlayer.soundRefs.map(ref=>ref.id)],
        comment:"Audio Malette Salon"
    },
]
