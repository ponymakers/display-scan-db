import {Howl, Howler} from 'howler';
import Logger from '@/utils/Logger';

export default class SoundPlayer {

    public static soundRefs = [
        {id:"complete_1", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie01.wav")})},
        {id:"complete_2", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie03.wav")})},
        {id:"complete_3", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie03.wav")})},
        {id:"complete_4", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie03.wav")})},
        {id:"complete_4", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie03.wav")})},

        {id:"lv_1", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/Bouton1.wav")})},
        {id:"lv_2", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/Bouton2.wav")})},
        {id:"lv_3", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/Bouton3.wav")})},
        {id:"lv_4", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/Bouton4.wav")})},
        {id:"lv_5", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/Bouton5.wav")})},
        {id:"lv_melodie", sound:new Howl({src:require("@/assets/sounds/living-room/washing-machine/micro_salon_melodie_sequence_comb.wav")})},
                
        {id:"micro_eco_energie01", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie01.wav")})},
        {id:"micro_eco_energie03", sound:new Howl({src:require("@/assets/sounds/micro_eco_energie03.wav")})},

        {id:"micro_validation_grande_eco", sound:new Howl({src:require("@/assets/sounds/common/validation/micro_validation_grande_eco.wav")})},
        {id:"micro_validation_moyenne_eco", sound:new Howl({src:require("@/assets/sounds/common/validation/micro_validation_moyenne_eco.wav")})},
        {id:"micro_validation_petite_eco", sound:new Howl({src:require("@/assets/sounds/common/validation/micro_validation_petite_eco.wav")})},
        {id:"micro_validation_bonus", sound:new Howl({src:require("@/assets/sounds/common/validation/micro_validation_grande_eco.wav")})},

        {id:"micro_qg_amb", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_amb.wav")})},
        {id:"micro_qg_bdd_digicode_bouton", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_bdd_digicode_bouton.wav")})},
        {id:"micro_qg_bdd_digicode_demande", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_bdd_digicode_demande.wav")})},
        {id:"micro_qg_bdd_erreur_code", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_bdd_erreur_code.wav")})},
        {id:"micro_qg_bdd_jauges_divers", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_bdd_jauges_divers.wav")})},
        {id:"micro_qg_bdd_valide_code_chargement", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_bdd_valide_code_chargement.wav")})},
        {id:"micro_qg_code", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_code.wav")})},
        {id:"micro_qg_micro_duplex", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_micro_duplex.wav")})},
        {id:"micro_qg_scanner_bouton_scan", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_scanner_bouton_scan.wav")})},
        {id:"micro_qg_scanner_bouton_tab", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_scanner_bouton_tab.wav")})},
        {id:"micro_qg_scanner_jauges_divers", sound:new Howl({src:require("@/assets/sounds/hq/micro_qg_scanner_jauges_divers.wav")})},

        {id:"micro_salon_amb", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_amb.wav")})},
        {id:"micro_salon_bouilloire", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_bouilloire.wav")})},
        {id:"micro_salon_bouton", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_bouton.wav")})},
        {id:"micro_salon_bouton_cafetiere_sfx", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_bouton_cafetiere_sfx.wav")})},
        {id:"micro_salon_bouton_pression", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_bouton_pression.wav")})},
        {id:"micro_salon_casserole", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_casserole.wav")})},
        {id:"micro_salon_casserole_sfx_ferme", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_casserole_sfx_ferme.wav")})},
        {id:"micro_salon_casserole_sfx_ouvert", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_casserole_sfx_ouvert.wav")})},
        {id:"micro_salon_chauffage_fuite", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_chauffage_fuite.wav")})},
        {id:"micro_salon_coupure_courant", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_coupure_courant.wav")})},
        {id:"micro_salon_frigo", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_frigo.wav")})},
        {id:"micro_salon_frigo_sfx_ferme", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_frigo_sfx_ferme.wav")})},
        {id:"micro_salon_frigo_sfx_ouvert", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_frigo_sfx_ouvert.wav")})},
        {id:"micro_salon_jauge_timer", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_jauge_timer.wav")})},
        {id:"micro_salon_lampes", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_lampes.wav")})},
        {id:"micro_salon_lavabo", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_lavabo.wav")})},
        {id:"micro_salon_remise_courant", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_remise_courant.wav")})},
        {id:"micro_salon_selecteur_rotatif", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_selecteur_rotatif.wav")})},
        {id:"micro_salon_telecommande_code", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_telecommande_code.wav")})},
        {id:"micro_salon_lave_vaiselle_sfx", sound:new Howl({src:require("@/assets/sounds/living-room/micro_salon_lave_vaiselle_sfx.wav")})},

        {id:"micro_chambre_lampes_interrupteur", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_lampes_interrupteur.wav")})},
        {id:"micro_chambre_lave_linge_selecteur", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_lave_linge_selecteur.wav")})},
        {id:"micro_chambre_lave_linge_sfx", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_lave_linge_sfx.wav")})},
        {id:"micro_chambre_ordinateur_chargement", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_ordinateur_chargement.wav")})},
        {id:"micro_chambre_ordinateur_chargement_fin", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_ordinateur_chargement_fin.wav")})},
        {id:"micro_chambre_ordinateur_clique", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_ordinateur_clique.wav")})},
        {id:"micro_chambre_ordinateur_erreur", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_ordinateur_erreur.wav")})},
        {id:"micro_chambre_ordinateur_validation", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_ordinateur_validation.wav")})},
        {id:"micro_chambre_robinet_fuite", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_robinet_fuite.wav")})},
        {id:"micro_chambre_seche_linge_mode_eco", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_seche_linge_mode_eco.wav")})},
        {id:"micro_chambre_seche_linge_sfx", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_seche_linge_sfx.wav")})},
        {id:"micro_chambre_thermostat_selecteur", sound:new Howl({src:require("@/assets/sounds/bedroom/micro_chambre_thermostat_selecteur.wav")})},
    ]

    public static play(message:string) {
        let ref = this.soundRefs.find(ref=>{return message.toString() == ref.id});
        if(ref){
            const howl = ref.sound;
            howl.play();
        }else{
            Logger.error("Sound ref not found on message", message)
        }
    }
    //================================================================================
    // Briefing
    //================================================================================
    
    
    //================================================================================
    // Chest
    //================================================================================
    
    
    //================================================================================
    // Energy label
    //================================================================================


    //================================================================================
    // Thermostat
    //================================================================================


    //================================================================================
    // Living room
    //================================================================================


    //================================================================================
    // Game
    //================================================================================


    //================================================================================
    // Audio
    //================================================================================


    //================================================================================
    // Content
    //================================================================================

}
