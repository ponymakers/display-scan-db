import Universe from '@/vo/Universe';


export default class conf{


    //================================================================================
    // MQTT
    //================================================================================
    public static get mqttBrokerUrl():string{
        //return process.env.MQTT_BROKER_URL || 'wss://test.mosquitto.org:8081';
        //return process.env.MQTT_BROKER_URL || 'ws://localhost:9001';
        //return process.env.MQTT_BROKER_URL || 'http://192.168.1.44:9001'; // MACRO
        return process.env.MQTT_BROKER_URL || 'http://192.168.1.43:9001'; // MICRO

    }


    //================================================================================
    // MQTT Topics
    //================================================================================
    public static localTest = false;
    public static universeId?:string = "0";

    public static get mqttTopic_DatabaseTabClick():string{
        return '/malette/'+this.universeId+'/database/tab/click';
    }

    public static get mqttTopic_DatabaseProductClick():string{
        return '/malette/'+this.universeId+'/database/product/click';
    }

    public static get mqttTopic_DatabaseProductPurchase():string{
        return '/malette/'+this.universeId+'/database/product/purchase';
    }

    public static get mqttTopic_ActivitySet():string{
        return '/malette/'+this.universeId+'/product/category/set';
    }

    public static get mqttTopic_ScanSend():string{
        return '/malette/'+this.universeId+'/scan/click';
    }

    public static get mqttTopic_ScanActivationState():string{
        return '/malette/'+this.universeId+'/scan/state';
    }

    public static get mqttTopic_ScanMessage():string{
        return '/malette/'+this.universeId+'/scan/message/:id';
    }

    public static get mqttTopic_ScanPin():string{
        return '/malette/'+this.universeId+'/lock/set';
    }

    public static get mqttTopic_LockPadSubmit():string{
        return '/malette/'+this.universeId+'/lock/submit';
    }

    public static get mqttTopic_LockSet():string{
        return '/malette/'+this.universeId+'/scan/pin/:pin';
    }

    public static mqttTopic_maletteMode(universeId):string{
        return '/malette/'+universeId+'/mode';
    }



    // * Envoyé par le backend pour changer l'état global des écrans du PC
    // pc-chambre/mode/activate or deactivate
    //     "optimisation"
    //     "co2"
    //     "avion"
    //     "veille"
    public static get mqttTopic_PC_mode_activate():string{
        return '/pc-chambre/mode/activate';
    }
    public static get mqttTopic_PC_mode_deactivate():string{
        return '/pc-chambre/mode/deactivate';
    }
    
    
    // * Envoyé par le front-end pour affirmer la réussite d'un des points
    // pc-chambre/success
    //     "optimisation"
    //     "co2"
    //     "avion"
    //     "veille
    
    public static get mqttTopic_PC_success():string{
        return '/pc-chambre/success';
    }

    public static get mqttTopic_PC_failure():string{
        return '/pc-chambre/failure';
    }


    static mqttTopic_ActionComplete: string = "activty/:universId/:activityId/done";
    
    
    
    // # Hardware MACRO
    // :pupitreId
    //     "pupitregauche"
    //     "pupitredroit"
    // :boutonId
    //     "bouton1"
    //     "bouton2"
    //     "bouton3"
    //     "bouton4"
    //     "bouton5"
    //     "bouton6"

    // * ???
    // /macro/:pupitreId/encoder/:boutonId/dispo
    //     ???
    public static mqttTopic_Pupitre_Encoder_Dispo(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/dispo';
    }

    // * Activation du bouton
    // /macro/:pupitreId/encoder/:boutonId/onoff
    //     1/0
    public static mqttTopic_Pupitre_Encoder_OnOff(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/onoff';
    }

    // * sens de rotation du bouton
    // /macro/:pupitreId/encoder/:boutonId/rot
    //     -1/1
    public static mqttTopic_Pupitre_Encoder_Rot(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/rot';
    }

    // * vitesse du bouton
    // /macro/:pupitreId/encoder/:boutonId/speed
    //     0-255
    public static mqttTopic_Pupitre_Encoder_Speed(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/speed';
    }

    // * valeur du bouton
    // /macro/:pupitreId/encoder/:boutonId/lum
    //     0-255
    public static mqttTopic_Pupitre_Encoder_Lum(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/lum';
    }

    // * Jauge
    // /macro/:pupitreId/jauge/:boutonId/
    //     0-5
    public static mqttTopic_Pupitre_Jauge(pupitreId:string):string{
        return '/macro/'+pupitreId+'/jauge/:boutonId';
    }

    // * picto
    // /macro/:pupitreId/scan/
    //     0-27
    public static mqttTopic_Pupitre_Scan(pupitreId:string):string{
        return '/macro/'+pupitreId+'/scan';
    }



    //================================================================================
    // Briefing
    //================================================================================
    // * Gestion des sources vidéos du briefing
    // * 'on' / 'off'
    public static get mqttTopic_briefing_tv_src():string{
        return '/tv-briefing/src';
    }
    
    //================================================================================
    // Chest
    //================================================================================
    public static mqttTopic_chest_mode(universeId:string):string{
        return `/coffre/${universeId}/mode`;
    }
    
    public static mqttTopic_chest_lock_click(universeId:string):string{
        return `/coffre/${universeId}/lock/click`;
    }
    
    public static mqttTopic_chest_gauge(universeId:string):string{
        return `/coffre/${universeId}/jauge`;
    }
    
    public static mqttTopic_chest_scan_working(universeId:string):string{
        return `/coffre/${universeId}/scan/working`;
    }
    
    public static mqttTopic_chest_action_complete(universeId:string):string{
        return `/coffre/${universeId}/action-complete`;
    }
    
    
    
    //================================================================================
    // Energy label
    //================================================================================
    public static get mqttTopic_energy_label_washing_machine():string{
        return '/energy-label/2';
    }
    
    public static get mqttTopic_energy_label_dishwasher():string{
        return '/energy-label/1';
    }
    
    public static get mqttTopic_energy_label_fridge():string{
        return '/energy-label/0';
    }

    public static mqttTopic_energy_label_index(index:string):string{
        return '/energy-label/'+index;
    }


    //================================================================================
    // Thermostat
    //================================================================================
    public static mqttTopic_thermostat_mode_programmation(universeId:string):string{
        return `/thermostat/${universeId}/mode/programmation`;
    }

    public static mqttTopic_thermostat_mode_temperature(universeId:string):string{
        return `/thermostat/${universeId}/mode/temperature`;
    }

    public static mqttTopic_thermostat_temperature(universeId:string):string{
        return `/thermostat/${universeId}/temperature`
    }

    //================================================================================
    // Living room
    //================================================================================
    // * Display television video
    // * 'on' / 'off'
    public static get mqttTopic_salon_tv_mode():string{
        return '/tv-salon/mode';
    }


    //================================================================================
    // Game
    //================================================================================

    static mqttTopic_GameTimestamp(): string {
        return '/game/end-timestamp';
    }
    static mqttTopic_PhaseTimestamp(universeId:Universe|string=":universeId"): string {
        return '/game/'+universeId+'/end-timestamp';
    }
    public static mqtt_ActiveActions(universId:string|Universe=":universId"): string {
        return "activties/"+universId+"/actives";
    }

    //================================================================================
    // Audio
    //================================================================================

    static mqttTopic_AudioRoom(universeId:Universe|string=":universeId"): string {
        return '/sound/room/'+universeId;
    }

    static mqttTopic_AudioMalette(universeId:Universe|string=":universeId"): string {
        return '/sound/malette/'+universeId;
    }

    //================================================================================
    // Content
    //================================================================================

    static frigoCat={
        name:"Réfirgérateur",
        showClassBtn:true,
        products:[
            {
                title:"Réfrigérateur BOSCH VITA FRESH",
                imgSrc:"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00377181_10_FRONT.png",
                caracteristics:[
                    "Volume 366 L",
                    "Dimensions HxLxP : 203x60x66 cm",
                    "Réfrigérateur à froid ventilé",
                    "Eclairage LED",
                ],
            },
            {
                title:"Réfrigérateur armoire LIEBHERR KEF4310",
                imgSrc:"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00373330_10_FRONT.png",
                caracteristics:[
                    "Volume 390 L",
                    "Dimensions HxLxP : 185x60x66.5 cm ",
                    "Froid brassé - Tout utile",
                    "Contrôle électronique",
                ],
            },
            {
                title:"Réfrigérateur Samsung RB 3 CJ 3000 SA",
                imgSrc:"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00373330_10_FRONT.png",
                caracteristics:[
                    "Volume 328 L",
                    "Dimensions HxLxP : 185x59.5x66.8 cm",
                    "Froid ventilé",
                    "Régulation électronique de la température",
                ],
            },
        ]
    }
    static laveVaisselleCat={
        name:"Lave-vaisselle",
        showClassBtn:true,
        products:[
            {
                title:"Lave-vaisselle Whirpool OWFC3C26X",
                imgSrc:"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00394297_10_FRONT.png",
                caracteristics:[
                    "Largeur : 60cm",
                    "Nombre de couverts : 14",
                    "Consommation d'eau par cycle : 9L",
                    "Départ différé : oui, 2/4/8h",
                ],
            },
            {
                title:"Lave-vaisselle Indesit DFG26B1NX",
                imgSrc:"https://d2olcigfo3zqnn.cloudfront.net/pub/media/catalog/category/electroplanet-565-lave-vaisselle.png",
                caracteristics:[
                    "Largeur : 60 cm",
                    "Nombre de couverts : 13",
                    "Consommation d'eau par cycle : 11 litres",
                    "Départ différé : 9 heures",
                ],
            },
            {
                title:"Lave-vaisselle Electrolux ESF8650ROW",
                imgSrc:"https://www.braultetmartineau.com/wcsstore/BMCatalogAssetStore/images/main/00396685_10_FRONT.png",
                caracteristics:[
                    "Largeur : 59.6 cm",
                    "Nombre de couverts : 15",
                    "Consommation d'eau par cycle : 11 litres",
                    "Départ différé : 24 heures",
                ],
            }
        ]
    }
    static laveLingeCat={

        name:"Lave-linge",
        showClassBtn:true,
        products:[
            {
                title:"Lave-linge Hublot Siemens iQ700",
                imgSrc:"https://www.electrolux.fr/remote.jpg.ashx?preset=16%3A9-2880&origin=T1&urlb64=aHR0cHM6Ly9zZXJ2aWNlcy5lbGVjdHJvbHV4LW1lZGlhbGlicmFyeS5jb20vMTE4ZWQ0YzBlZTY1NDZmNGE3Njg0YzdmZWY4Yzk4NWFOclptWWtNODYxZDFmL3ZpZXcvV1NfUE4vUFNFRVdNMTgwUEEwMDAyRC5wbmc&hmac=tb-yvtpz-UA",
                caracteristics:[
                    "Capacité de lavage – 9 kg",
                    "Essorage : 1400 tr/min",
                    "Consommation d’eau : 11 220 L / an",
                    "Consommation : 152 kWh / an",
                ],
            },
            {
                title:"Lave-linge Hublot Asko W6984S",
                imgSrc:"https://www.electrolux.fr/remote.jpg.ashx?preset=16%3A9-2880&origin=T1&urlb64=aHR0cHM6Ly9zZXJ2aWNlcy5lbGVjdHJvbHV4LW1lZGlhbGlicmFyeS5jb20vMTE4ZWQ0YzBlZTY1NDZmNGE3Njg0YzdmZWY4Yzk4NWFOclptWWtNODYxZDFmL3ZpZXcvV1NfUE4vUFNFRVdNMTYwUDE2MzAwMy5wbmc&hmac=NwpI8b4jwIY",
                caracteristics:[
                    "Capacité de lavage – 8 kg",
                    "Essorage : 1800 tr/min",
                    "Consommation d’eau : 10 340 L / an",
                    "Consommation : 192 kWh / an",
                ],
            },
            {
                title:"Lave-linge Hublot Gorenje W75F44/I",
                imgSrc:"https://www.magarantie5ans.fr/19235-home_default/lave-linge-samsung-ww10m86gnoa-quickdrive.jpg",
                caracteristics:[
                    "Capacité de lavage – 7 kg",
                    "Essorage : 1400 tr/min",
                    "Consommation d’eau : 9523 L / an",
                    "Consommation : 150 kwH /an",
                ],
            }
        ]
    }

    static voitureCat={

        name:"Voitures",
        showClassBtn:false,
        products:[
            {
                title:"Citroën C3 AIRCROSS ",
                imgSrc:"https://media.citroen.fr/image/18/4/c3aircross-thumbnail.257184.png",
                caracteristics:[
                    "CO2 (g / km) : 146",
                    "Classe CO2 : D",
                    "Bonus (-) / Malus (+) : + 1153 euros",
                    "",""
                ],
            },
            {
                title:"PEUGEOT 308 GTi",
                imgSrc:"https://www.perthcitypeugeot.com.au/wp-content/uploads/2018/09/308-Peugeot-GTi.png",
                caracteristics:[
                    "CO2 (g / km) : 139",
                    "Classe CO2 : C",
                    "Bonus (-) / Malus (+) : +613 euros", "",""
                ],
            },
            {
                title:"Renault ZOE - Gamme 2017",
                imgSrc:"https://www.autodiscount.fr/images/voitures/defaut/std/5355-std.png",
                caracteristics:[
                    "CO2 (g / km) : 0",
                    "Classe CO2 : A",
                    "Bonus (-) / Malus (+) : - 6000 euros", "",""
                ],
            },
        ]
    }


    public static productsCategories=[
        conf.frigoCat,
        conf.laveLingeCat,
        conf.voitureCat,
        conf.laveVaisselleCat,
    ]

    public static recos = [
        {desc:"N'utiliser que l'eau nécessaire.", hint:"Contenance maximale de l'objet en millilitres"},
        {desc:"Ne lancer l'appareil qu’une fois plein.", hint:"W + n° du modèle de l'appareil + T"},
        {desc:"Dégivrer régulièrement le frigo.", hint:"HV (dans le plat préféré de Clara)"},
        {desc:"Utiliser une casserole sur une plaque à induction.", hint:"B + nombre de volts de l'objet à remplacer"},
        {desc:"Lancer la machine une fois qu'elle est pleine.", hint:"Référence de la machine lue à l'envers"},
        {desc:"Partager son véhicule avec d'autres personnes.", hint:"N°identifiant dans le courrier Covoit'"},
        {desc:"Ne l’utiliser que lorsqu’il fait trop froid ou humide.", hint:"Référence de l'appareil"},
        {desc:"Réparer le lavabo.", hint:"L + numéro de l'appartement voisin. "},
        {desc:"Remplacer son ballon d'eau chaude.", hint:"9885"},
        {desc:"Renforcer l'isolation des fenêtres.", hint:"W + numéro de série + T"},
        {desc:"Se séparer du gadget.", hint:"L8Y4"},
        {desc:"Enlever les ampoules.", hint:"A + température de la pièce"},
        {desc:"Laisser la porte du frigo fermée", hint:"HO + température du frigo"},
        {desc:"Changer le pommeau de douche.", hint:"Référence du pommeau lue à l'envers"},
        {desc:"Isoler les murs de la chambre.", hint:"841237"},
        {desc:"Utiliser de la domotique.", hint:"9ème lettre de l'alphabet + Robot"},
        {desc:"Augmenter l'intensité lumineuse des lampes.", hint:"L + 5"},
        {desc:"Programmer son réveil.", hint:"AF737"},
        {desc:"Débrancher les appareils en veille.", hint:"W55T"},
        {desc:"Garder les volets fermés.", hint:"numéro de série + TT"},
        {desc:"Monter les stores en journée.", hint:"S + nombre de volets"},
        {desc:"Nettoyer sa cafetière.", hint:"C + voltage maximum de l'appareil"},
        {desc:"Voyager en transports en commun.", hint:"N° de la carte d'abonnement"},
        {desc:"Eteindre les lumières si elles ne sont pas utilisées.", hint:"L + nombre d'ampoules dans la pièce"},
    ]
}
