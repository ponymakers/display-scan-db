import { STATES } from '../states/headquarter';
import PubSub from '@/controllers/PubSub';
import conf from '@/config/conf';

export default {
    data() {
        return {
            states: STATES,
            currentState: STATES.ENIGMA_1,
        }
    },
    
    mounted() {
        PubSub.sub(conf.mqttTopic_maletteMode(this.universeId), (e) => this.setCurrentState(e.payload));

        document.addEventListener('keydown', (e) => {
            if (e.which === 49) this.setCurrentState(this.states.ENIGMA_1);
            if (e.which === 50) this.setCurrentState(this.states.ENIGMA_2);
            if (e.which === 51) this.setCurrentState(this.states.WAITING);
            if (e.which === 52) this.setCurrentState(this.states.GAME);
            if (e.which === 53) this.setCurrentState(this.states.END);

        })
    },
    methods: {
        setCurrentState(state) {
            this.currentState = state;
        }
    }
}
