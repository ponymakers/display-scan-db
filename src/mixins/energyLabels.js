export default {
    data() {
        return {
            currentChoice: 3
        }
    },
    methods: {
        setCurrentChoice(value) {
            this.currentChoice = value;
        }
    }
}
