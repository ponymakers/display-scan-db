import Vue from 'vue'
import Component from 'vue-class-component'
import PubSub from '@/controllers/PubSub'
import conf from '@/config/conf'

@Component<ThermostatMixin>({
    mounted() {
        PubSub.sub(conf.mqttTopic_thermostat_mode_temperature(this.universeId), (event) => {
            switch ((event.payload) as 'on' | 'off') {
                case 'on':
                    this.currentMode = 'temperature'
                    break;
                case 'off':
                    if (this.currentMode !== 'programmation') {
                        this.currentMode = 'programmation'
                    }
                    break;
            }
        })
        PubSub.sub(conf.mqttTopic_thermostat_mode_programmation(this.universeId), (event) => {
            switch ((event.payload) as 'on' | 'off') {
                case 'on':
                    this.currentMode = 'programmation'
                    break;
                case 'off':
                    if (this.currentMode !== 'programmation') {
                        this.currentMode = 'temperature'
                    }
                    break;
            }
        })
    }
})
export default class ThermostatMixin extends Vue {
    public tabs = [{id: 'programmation', name: "Programmation"}, {id: 'temperature', name: "Thermostat"}];
    public universeId = "0"
    public currentMode: 'programmation' | 'temperature' = 'programmation';
    public days = [
        "Lundi",
        "Mardi",
        "Mercredi",
        "Jeudi",
        "Vendredi",
        "Samedi",
        "Dimanche"
    ];
    public programmationMatrix: boolean[][] = [[]]

    public get programmation() {
        return this.programmationMatrix.reduce((acc, slots) => {
            return `${acc}${slots.map(slot => slot ? "1" : "0").join("")}`
        }, "")
    }

    public save() {
        PubSub.pub("/thermostat/" + this.universeId + "/programmation", this.programmation);
    }

    public get currentTab(): { id: string, name: string } {
        return this.tabs.find(tab => tab.id === this.currentMode)!
    }
}
