import { STATES } from '../states/chest';

export default {
    data() {
        return {
            states: STATES,
            currentState: STATES.LOCKED
        }
    },
    methods: {
        setCurrentState(state) {
            this.currentState = state;
        }
    }
}
