export enum UIEvents {
    ShowWindowCloseButton = 'ui:window:show-close-button',
    HideWindowCloseButton = 'ui:window:hide-close-button'
}
